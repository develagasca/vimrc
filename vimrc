set nocompatible  " break backward-compatibility with Vi

set backspace=indent,eol,start
set number
set ruler
set showcmd
set tabstop=4 shiftwidth=4 expandtab

filetype plugin indent on

syntax on

" only show the colorcolumn when text reaches the columns 73 and 80+
call matchadd('ColorColumn', '\(\%73v\|\%>79v.\+\)')

colorscheme desert

" automatically remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" enable this on Windows
"if has("gui_running")
"    autocmd GUIEnter * set vb t_vb=
"    if has("gui_win32")
"        set guifont=Consolas:h11:cANSI
"    endif
"endif
